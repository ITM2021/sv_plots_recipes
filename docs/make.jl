using SVPlotsRecipes
using Documenter

DocMeta.setdocmeta!(SVPlotsRecipes, :DocTestSetup, :(using SVPlotsRecipes); recursive=true)

makedocs(;
    modules=[SVPlotsRecipes],
    authors="Matthew Camp",
    repo="https://github.com/mcamp/SVPlotsRecipes.jl/blob/{commit}{path}#{line}",
    sitename="SVPlotsRecipes.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://mcamp.gitlab.io/SVPlotsRecipes.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
