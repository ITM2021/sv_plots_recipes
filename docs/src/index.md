```@meta
CurrentModule = SVPlotsRecipes
```

# SVPlotsRecipes

Documentation for [SVPlotsRecipes](https://github.com/mcamp/SVPlotsRecipes.jl).

```@index
```

```@autodocs
Modules = [SVPlotsRecipes]
```
