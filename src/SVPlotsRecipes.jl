module SVPlotsRecipes
using SpatialVoting
using Plots

"""
Helper functions to calculate the size and location of grid cells from SVGrid/SVCell objects
"""
Plots.Shape(w, h, x, y) = Plots.Shape(x .+ [0, w, w, 0, 0], y .+ [0, 0, h, h, 0])
function Plots.Shape(svcell::SVCell) 
    cell_corners = corners(svcell)
    if occursin(".",svcell.key)
        return Shape(
              Float64( abs( first(svcell.parent_xrange) - last(svcell.parent_xrange) ) / (length(svcell.parent_xrange)-1) ),
              Float64( abs( first(svcell.parent_yrange) - last(svcell.parent_yrange) ) / (length(svcell.parent_yrange)-1) ),
              lower_left_corner(svcell)...)
    else # its a top level svcell in an dysvgrid
        return Shape(
              Float64( abs( first(svcell.parent_xrange) - last(svcell.parent_xrange) ) ),
              Float64( abs( first(svcell.parent_yrange) - last(svcell.parent_yrange) ) ),
              lower_left_corner(svcell)...)
    end
end
Plots.Shape(svgrid::SVGrid) = Shape(Float64(abs( first(svgrid.x) - last(svgrid.x) / (length(svgrid.x)-1) )),
                                    Float64(abs( first(svgrid.y) - last(svgrid.y) / (length(svgrid.y)-1) )),
                                    lower_left_corner(svgrid)...
                                   )

"""
A Plots.jl recipe that will plot a `DynamicSVGrid` as a bunch of rectangles instead of using a `heatmap`
This is done so we can control the color of the cells based on their counts and we can also plot
the raw `AllCompressiveEncodingStats` on top of the `SVGrid` and things will line up
"""
Plots.@recipe function f(dysvgrid::DynamicSVGrid;plot_grids=false,colors=SVColorScheme(minimum(dysvgrid),maximum(dysvgrid)))
    xguide --> Symbol(dysvgrid.x_type)
    yguide --> Symbol(dysvgrid.y_type)
    sv = first(dysvgrid.grid)[2]
    if typeof(sv) <: SVGrid
        x_range = first(dysvgrid.grid)[2].x
        y_range = first(dysvgrid.grid)[2].y
    else
        x_range = first(dysvgrid.grid)[2].parent_xrange
        y_range = first(dysvgrid.grid)[2].parent_yrange
    end
    xcell_size = Float64(abs(minimum(x_range)-maximum(x_range))/length(x_range))
    ycell_size = Float64(abs(minimum(y_range)-maximum(y_range))/length(y_range))
    sv_corners = corners(dysvgrid)
    title  --> "DynamicSVGrid"
    background_color --> colors[0]
    label := false
    aspect_ratio := :equal

    for xy=keys(dysvgrid.grid)
        if typeof(dysvgrid.grid[xy]) <: SVCell
            current_cell = dysvgrid.grid[xy]
            cell_shape = Plots.Shape(current_cell)
            @series begin
                seriescolor  := colors[current_cell.count]
                label --> current_cell.key
                legend := false
                # hover := current_cell.key
                name := current_cell.key
                customedata := current_cell.key
                type := "shape"
                cell_shape
            end
        else
            svgrid = dysvgrid.grid[xy]
            if plot_grids == false
                cell_shape = Plots.Shape(svgrid)
                @series begin
                    seriescolor := colors[svgrid.count]
                    label --> svgrid.parent
                    legend := false
                    # hover := svgrid.parent
                    name := svgrid.parent
                    customedata := svgrid.parent
                    type := "shape"
                    cell_shape
                end
            else
                for ix=1:length(svgrid.grid)
                    if typeof(svgrid.grid[ix]) == SVCell
                        current_cell = svgrid.grid[ix]
                        cell_shape = Plots.Shape(current_cell)
                    else
                        xy = CartesianIndices((length(svgrid.x),length(svgrid.y)))[ix]
                        x,y = xy[1],xy[2]
                        parent_key = svgrid.parent
                        current_cell = SVCell([],nothing,"$parent_key.$ix",svgrid.x,svgrid.y,0)
                        cell_shape = Plots.Shape(current_cell)
                    end
                    @series begin
                        seriescolor  := colors[current_cell.count]
                        label --> current_cell.key
                        legend := false
                        # hover := current_cell.key
                        name := current_cell.key
                        customedata := current_cell.key
                        type := "shape"
                        cell_shape
                    end
                end
            end
        end
    end
    primary := false
    ()
end

Plots.@recipe function f(svcell::SVCell;colors=SVColorScheme(minimum(svcell.count-1),maximum(svcell.count+1)))
    xticks --> length(svcell.parent_xrange)
    yticks --> length(svcell.parent_yrange)
    title  --> "SVCell"
    background_color --> colors[0]
    label := false
    aspect_ratio := :equal

    @series begin
    cell_shape = Plots.Shape(svcell)
    seriescolor := colors[svcell.count]
    label --> svcell.key
    legend := false
    # hover := svcell.key
    name := svcell.key
    cell_shape
    end
    primary := false
    ()
end

"""
A Plots.jl recipe that will plot a `SVGrid` as a bunch of rectangles instead of using a `heatmap`
This is done so we can control the color of the cells based on their counts and we can also plot
the raw `AllCompressiveEncodingStats` on top of the `SVGrid` and things will line up
"""
Plots.@recipe function f(svgrid::SVGrid;colors=SVColorScheme(minimum(svgrid),maximum(svgrid)))
    xguide --> Symbol(svgrid.x_type)
    yguide --> Symbol(svgrid.y_type)
    xticks --> length(svgrid.x)
    yticks --> length(svgrid.y)
    title  --> "SVGrid"
    background_color --> colors[0]
    label := false
    aspect_ratio := :equal

    cell_shapes = []
    for ix=1:length(svgrid.grid)
        @series begin
            if typeof(svgrid.grid[ix]) == SVCell
                current_cell = svgrid.grid[ix]
                cell_shape = Plots.Shape(current_cell)
            else
                xy = CartesianIndices((length(svgrid.x),length(svgrid.y)))[ix]
                x,y = xy[1],xy[2]
                parent_key = svgrid.parent
                current_cell = SVCell([],nothing,"$parent_key.$ix",svgrid.x,svgrid.y,0)
                cell_shape = Plots.Shape(current_cell)
            end
            seriescolor := colors[current_cell.count]
            label --> current_cell.key
            legend := false
            # hover := current_cell.key
            name := current_cell.key
            cell_shape
        end
    end
    primary := false
    ()
end


Plots.@recipe function f(svgrid::SVGrid, svdata::SVData;colors=SVColorScheme())
    # DataFrame([(key1=k1,key2=k2) for (k1,k2) in split.(svdata[!,"key"],".")])
    x_range = svgrid.x
    y_range = svgrid.y
    x_half_step = Float64(abs(minimum(x_range)-maximum(x_range))/length(x_range)) / 2
    y_half_step = Float64(abs(minimum(y_range)-maximum(y_range))/length(y_range)) / 2
    data = filter(row-> between(row[svgrid.x_type],svgrid.x) && between(row[svgrid.y_type],svgrid.y),svdata.v)
    # cause heatmap is off by half
    data[!,svgrid.x_type] = data[!,svgrid.x_type] .- x_half_step
    data[!,svgrid.y_type] = data[!,svgrid.y_type] .- y_half_step

    plot(svgrid)

    scat = Plots.plot!(data[!,svgrid.x_type],
         data[!,svgrid.y_type],
            seriestype=:scatter,
            background=:black,
            xlims=svgrid.x,
            ylims=svgrid.y,
            # xlims=(
            #        first(svgrid.x)-x_half_step,svgrid.x[end]+x_half_step
            #       ),
            # ylims=(
            #        first(svgrid.y)-y_half_step,svgrid.y[end]+y_half_step
            #       ),
            xticks=length(svgrid.x),
            yticks=length(svgrid.y),
            xlabel=Symbol(svgrid.x_type), 
            ylabel=Symbol(svgrid.y_type),
            title=svgrid.exemplar,
           )
    
    return ()
end

function Plots.heatmap(dysvgrid::DynamicSVGrid;title::String="DynamicSVGrid")
    # TODO: Plot all the grids one layer down too
    xmax = maximum([xy[1] for xy in keys(dysvgrid.grid)])
    xmin = minimum([xy[1] for xy in keys(dysvgrid.grid)])
    ymax = maximum([xy[2] for xy in keys(dysvgrid.grid)])
    ymin = minimum([xy[2] for xy in keys(dysvgrid.grid)])
    max = maximum([xmax,ymax]) + 1
    xstep = first(dysvgrid.grid)[2].x.step
    ystep = first(dysvgrid.grid)[2].y.step
    # xbins = first(dysvgrid.grid)[2].x.len
    # ybins = first(dysvgrid.grid)[2].y.len
    gdata = zeros(max,max)
    for k=keys(dysvgrid.grid)
        x = k[1] + 1
        y = k[2] + 1
        gdata[x,y] = dysvgrid.grid[k].count
    end
    heatmap(1:max,
            1:max, gdata',
        c=cgrad([:black, :grey, :blue,:yellow, :red]),
        xlabel=dysvgrid.x_type, ylabel=dysvgrid.y_type,
        title=title)
end

function Plots.heatmap(svgrid::SVGrid;title::String="SVGrid")
    gdata = grid(svgrid) |> Array
    x_half_step = Float64(svgrid.x.step) / 2
    y_half_step = Float64(svgrid.y.step)
    xrange = range(svgrid.x[1]-x_half_step,svgrid.x[end]+x_half_step,step=2x_half_step)
    yrange = range(svgrid.y[1]-y_half_step,svgrid.y[end]+y_half_step,step=2y_half_step)
    hmap = heatmap(
            xrange,
            yrange,
            gdata',
            c=cgrad([:black, :blue,:yellow, :red]),
            xlabel=Symbol(svgrid.x_type), 
            ylabel=Symbol(svgrid.y_type),
            title=title,
            xlims=(
                   first(svgrid.x),svgrid.x[end]
                  ),
            ylims=(
                   first(svgrid.y),svgrid.y[end]
                  )
           )
    vline!(svgrid.x,color=:white,legend=false)
    hline!(svgrid.y,color=:white,legend=false)
    return hmap
end


function plot_term(svgrid::SVGrid;title::String="SVGrid")
    gdata = grid(svgrid) |> Array
    x = svgrid.x_type
    y = svgrid.y_type
    heatmap(gdata,height=50,width=50, xlabel="$x",ylabel="$y",xlim=[1,length(svgrid.x)-1],ylim=[1,length(svgrid.y)-1])
end

function plot_labels(svgrid::SVGrid,keyvals,labels;title::String="SVGrid with Labels")
    gdata = get_label_grid(svgrid.grid,keyvals,length(svgrid.x)-1,length(svgrid.y)-1,labels) |> Array
    heatmap(1:length(svgrid.x)-1,
            1:length(svgrid.y)-1, gdata',
        c=cgrad([:black, :blue,:yellow, :red, :purple]),
        xlabel=svgrid.x_type, ylabel=svgrid.y_type,
        title=title)
end

"""
This will plot a `SVGrid` that is within a `DynamicSVGrid` with a scatter plot of the `AllCompressiveEncodingStats` 
ideally you want to use `Plotly` backend but I think its safe to use with other plotting backends.
"""
function svgrid_scatter(dysvgrid::DynamicSVGrid,svdata::SVData,grid_x,grid_y; hover=svdata.label_cols)
    if (grid_x, grid_y) in keys(dysvgrid.grid)
        thegrid = dysvgrid.grid[grid_x,grid_y] # an x by y Array
        cell_data = hcat(svdata.v, DataFrame([(key1 = k1, key2 = k2) for (k1, k2) in split.(svdata[!,"key"], ".")]))
        lookup_key = "$grid_x-$grid_y"
        cell_data = filter(x -> x.key1 == lookup_key, cell_data)
        pdata = cell_data[!,[dysvgrid.x_type,dysvgrid.y_type]]
        # pdata = [convert(Array,x) for x in collect(eachrow(pdata))]
        gp = plot(thegrid,customdata=[string(x |> Array) for x in eachrow(cell_data[!,hover])],aspect_ratio=:equal);
        # @show String.(cell_data[!,hover]) |> Array
        hover_text = [string(x |> Array) for x in eachrow(cell_data[!,hover])]
        plot!(pdata[!,dysvgrid.x_type], pdata[!,dysvgrid.y_type], hover=hover_text,seriestype=:scatter, title="$grid_x,$grid_y"); 
    else
        gp = Plots.plot(title="No Data", seriestype=:scatter, customdata=[],aspect_ratio=:equal)
    end
    return gp
end

export svgrid_scatter

end
