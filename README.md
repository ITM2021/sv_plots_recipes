# SVPlotsRecipes

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://mcamp.gitlab.io/SVPlotsRecipes.jl/dev)
[![Build Status](https://github.com/mcamp/SVPlotsRecipes.jl/badges/master/pipeline.svg)](https://github.com/mcamp/SVPlotsRecipes.jl/pipelines)
[![Coverage](https://github.com/mcamp/SVPlotsRecipes.jl/badges/master/coverage.svg)](https://github.com/mcamp/SVPlotsRecipes.jl/commits/master)
[![Coverage](https://codecov.io/gh/mcamp/SVPlotsRecipes.jl/branch/master/graph/badge.svg)](https://codecov.io/gh/mcamp/SVPlotsRecipes.jl)
